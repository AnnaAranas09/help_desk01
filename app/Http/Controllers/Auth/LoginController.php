<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function index()
    {
        return view('pages.login', [

        ]);
    }

    public function login()
    {
        return output($request->get('email'));
        
        $user = User::where('email', $request->get('email'))->first();

        if(!count($user)){
            
            Session::flash('message', 'User not found'); 
            Session::flash('alert-class', 'alert-danger'); 

            return response()->json([
                'err' => 1,
                'msg' => 'error',
            ]);

            $user = new User;
            $user->first_name = $request->get('first_name');
            $user->last_name = $request->get('last_name');
            $user->email = $request->get('email');
            $user->profile_picture = $request->get('profile_picture');
            // $user->online_status = 1;
            // $user->account_type = 3;
            // $user->created_by = 1;
            // $user->account_status = 1;
            // $user->upload_limit = 100;
            // $user->price_per_lead_added = 0.01;
            // $user->price_per_lead_booked = 10;
            $user->save();
        }

        // if($user->account_status != 1)
        // {
        //     Auth::logout();
        //     Session::flush();
        //     Session::flash('message', 'User Inactive'); 
        //     Session::flash('alert-class', 'alert-danger'); 

        //     return response()->json([
        //         'err' => 1,
        //         'msg' => 'error',
        //     ]);
        // }

        // $user->online_status = 1;
        // $user->save();

        Auth::loginUsingId($user->id);

        // // $user->online

        return response()->json([
            'err' => 0,
            'msg' => 'success',
        ]);
    }
}
