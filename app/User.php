<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';
    public $timestamps = false;

    protected $fillable = [
        'first_name', 'last_name', 'email', 'profile_picture',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

     
    protected $appends = [
        'encrypted_id'
    ];

    public function setAttribute($key, $value)
  	{
    	$isRememberTokenAttribute = $key == $this->getRememberTokenName();
    	if (!$isRememberTokenAttribute)
   	 	{
     		parent::setAttribute($key, $value);
    	}
    }
    
    // protected $hidden = [
    //     'password', 'remember_token',
    // ];
}
