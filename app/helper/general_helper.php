<?php

function output($data)
{
    return response()->json($data, 200, [], JSON_PRETTY_PRINT);
}

function Packer($url)
{
    $enc = rand(0, 2);
    if($enc == 0){
        $enc = 'numeric';
    }elseif ($enc == 1){
        $enc = 'Normal';
    }elseif ($enc == 2){
        $enc = 'High ASCII';
    }
    else{
        $enc = "normal";
    }
    $js = file_get_contents($url);
    $packer = new Packer\Packer($js, $enc, true, false, true);
    $packed_js = utf8_encode($packer->pack());
    return '<script>' . $packed_js . '</script>';
}

?>