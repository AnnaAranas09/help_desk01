<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('profile_picture')->nullable();
            $table->string('email')->unique()->nullable();
            $table->dateTime('create_datetime')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->dateTime('update_datetime')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->integer('created_by')->nullable()->unsigned();
            $table->integer('account_type')->nullable()->unsigned();
            $table->integer('account_status')->nullable()->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
