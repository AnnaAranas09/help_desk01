var APP_URL = 'http://www.helpdesk.com/';

function pagination(func, current_page) {
	// console.log(current_page);
	$('ul[class="pagination"] li a[href]').removeAttr('href');
	$('ul[class="pagination"] li a').attr('onclick', func + '($(this).text())');
	current_page = $('ul[class="pagination"] li[class="active"]').length
		? parseInt($('ul[class="pagination"] li[class="active"]').text())
		: 1;
	$('ul[class="pagination"] li a[rel="prev"]').attr('onclick', func + '(' + (current_page - 1) + ')');
	$('ul[class="pagination"] li a[rel="next"]').attr('onclick', func + '(' + (current_page + 1) + ')');

	// console.log(current_page);
	return current_page;
}

var ajaxisRunning = false;
function ajaxRequest(url, data) {
	if (ajaxisRunning) {
		return;
	}

	ajaxisRunning = true;
	var res = $.ajax({
		type: 'POST',
		url: url,
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		data: data,
		success: function() {
			ajaxisRunning = false;
		},
		error: function() {
			ajaxisRunning = false;
		}
	});

	return res;
}

var ajaxisRunning = false;
function ajaxFormRequest(url, form) {
	if (ajaxisRunning) {
		return;
	}

	ajaxisRunning = true;

	var res = $.ajax({
		type: 'POST',
		url: url,
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		data: new FormData(form[0]),
		contentType: false,
		cache: false,
		processData: false,
		success: function() {
			ajaxisRunning = false;
		},
		error: function() {
			ajaxisRunning = false;
		}
	});

	return res;
}

function parseValidationMessage(res) {
	var msg = '';
	$.each(res, function(index, value) {
		msg = msg + '<li>' + value[0] + '</li>';
	});

	return msg;
}


