var APP_URL = 'http://www.helpdesk.com/';

function onSignIn(googleUser) 
{
    console.log('done');
    var profile = googleUser.getBasicProfile();
    
    var res = ajaxRequest(APP_URL + 'login', {
        email: profile.getEmail(),
        first_name: profile.getGivenName(),
        last_name: profile.getFamilyName(),
        profile_picture: profile.getImageUrl(),
	});
	res.done(function(res) {
        if (res['err'] == '1'){
            signOut();
        }
        console.log('loggedin');
        window.location.reload();
	});
    }

function signOut() 
{
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        console.log('User signed out.');
    });
}