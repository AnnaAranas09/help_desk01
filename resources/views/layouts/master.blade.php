<!doctype html>
<html lang="en" dir="ltr">
  <head>
    @include('layouts.header')
  </head>
  
  <body class="">
    <div class="page">
      <div class="page-main">
        <div class="header py-4">
          <div class="container">
            <div class="d-flex">
              <a class="header-brand" href="./index.html">
                <img src="{{ asset('public/tabler.admin/demo/brand/tabler.svg') }}" class="header-brand-img" alt="tabler logo">
              </a>
              <div class="d-flex order-lg-2 ml-auto">
              
                <div class="dropdown">
                  <a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
                    <span class="avatar" style="background-image: url({{ asset('public/tabler.admin/demo/faces/female/25.jpg')}})"></span>
                    <span class="ml-2 d-none d-lg-block">
                      <span class="text-default">Jane Pearson</span>
                      <small class="text-muted d-block mt-1">Administrator</small>
                    </span>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                    <a class="dropdown-item" href="#">
                      <i class="dropdown-icon fe fe-user"></i> Profile
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">
                      <i class="dropdown-icon fe fe-log-out"></i> Sign out
                    </a>
                  </div>
                </div>
              </div>
              <a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0" data-toggle="collapse" data-target="#headerMenuCollapse">
                <span class="header-toggler-icon"></span>
              </a>
            </div>
          </div>
        </div>
        <div class="header collapse d-lg-flex p-0" id="headerMenuCollapse">
          <div class="container">
            <div class="row align-items-center">

              <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                  <li class="nav-item">
                    <a href="/admin/messages" class="nav-link active"><i class="fe fe-home"></i> Messages</a>
                  </li>
   
                  <li class="nav-item">
                    <a href="/admin/issues" class="nav-link"><i class="fe fe-calendar"></i> Issues</a>
                  </li>

                  <li class="nav-item">
                    <a href="/admin/faq"" class="nav-link"><i class="fe fe-file-text"></i> FAQ</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="my-3 my-md-5">
            <div class="container">

              <div class="row row-cards">
                <div class="col-lg">
                  <div class="card" style="padding:1%"> 
                    
                        @yield('content')
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>

      @include('layouts.footer')

    </div>
  </body>


{{-- <script src="{{ asset('public/Bootstrap/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('public/Bootstrap/popper.min.js') }}"></script>
<script src="{{ asset('public/Bootstrap/bootstrap.min.js') }}"></script> --}}
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</html>