@extends('layouts.master')

@section('content')
<link rel="stylesheet" href="{{ asset('public/css/accordion.css') }}">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<center><h1 class="page-title">FAQ</h1></center>
<div class="row">
        <div class="col-md-12">
            <div class="panel-group" id="accordion" role="tablist" >

                @php
                $a =1;
            
                while ($a < 5){
                    
                @endphp
                <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading{{$a}}">
                            <h4 class="panel-title">
                                
                                <a class="collapsed" role="button" data-toggle="collapse"  href="#collapse{{$a}}" aria-expanded="false" aria-controls="collapse{{$a}}">
                                    Section {{$a}}
                                  
                                </a>  
                            </h4>
                        </div>
                        <div id="collapse{{$a}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$a}}" >
                            <div class="panel-body">
                                
                                    @include('pages.admin.panel-body')
                            </div>
                        </div>
                </div>
                @php
                $a++;
                }
                @endphp
            </div>
        </div>
    </div>

@endsection