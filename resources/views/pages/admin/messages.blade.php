@extends('layouts.master')

@section('content')
@php
  //echo Packer(env('APP_URL').'/public/js/global.js');
  echo Packer(env('APP_URL').'/public/js/message.js');
@endphp

<link rel="stylesheet" href="{{ asset('public/css/thread.css') }}">
<style>
.card{
    margin: 0px;
}
#seen{
    margin-top: 5px;
}
#accordion :hover, #accordion :focus{
    text-decoration:none;
    }
.collapse{
  padding-bottom: 25px;
} 
</style>
<center><h1 class="page-title">Messages</h1></center>
{{-- <div id="accordion">
    
    <div class="card">
        <div class="card-status"></div>
        <div class="card-header" id="headingOne">
            <h5 class="mb-0">
                <span class="avatar avatar-md" style="background-image: url({{ asset('public/tabler.admin/demo/faces/female/25.jpg')}})"></span>
                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    Firstname Lastname
                </button>
            </h5>
            <div class="card-options" id="cardOpt">
                <div class="d-flex align-items-center px-2">
                    <div>
                        <center><div><span class="tag tag-cyan">4 Messages</span></div></center>
                        <div class="avatar-list avatar-list-stacked" id="seen">
                                <span class="avatar avatar-sm" style="background-image: url({{ asset('public/tabler.admin/demo/faces/female/12.jpg')}})"></span>
                                <span class="avatar avatar-sm" style="background-image: url({{ asset('public/tabler.admin/demo/faces/female/21.jpg')}})"></span>
                                <span class="avatar avatar-sm" style="background-image: url({{ asset('public/tabler.admin/demo/faces/female/29.jpg')}})"></span>
                                <span class="avatar avatar-sm" style="background-image: url({{ asset('public/tabler.admin/demo/faces/female/2.jpg')}})"></span>
                                <span class="avatar avatar-sm" style="background-image: url({{ asset('public/tabler.admin/demo/faces/male/34.jpg')}})"></span>
                                <span class="avatar avatar-sm">+8</span>
                        </div>
                    </div>
                </div>         
            </div>
        </div>
  
      <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
        <div class="card-body">
            <div class="message-box">
                <div class="heading">
                    <span class="date1">08:39</span>
                </div>                                                                                           
                <span class="more">
                  reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur
                    sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut 
                  labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                  ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
                  reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur
                    sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </span>
            </div>   
        </div>
        
      </div>
    </div>

    <div class="card">
      
      <div class="card-header" id="headingTwo">
        <h5 class="mb-0">
            <span class="avatar avatar-md" style="background-image: url({{ asset('public/tabler.admin/demo/faces/female/1.jpg')}})"></span>
          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
            Collapsible Group Item #2
          </button>
        </h5>
        <div class="card-options" id="cardOpt">
                <div class="d-flex align-items-center px-2">
                    <div>
                        <center><div><span class="tag tag-cyan">3 Messages</span></div></center>
                        <div class="avatar-list avatar-list-stacked" id="seen">
                                <span class="avatar avatar-sm" style="background-image: url({{ asset('public/tabler.admin/demo/faces/female/12.jpg')}})"></span>
                                <span class="avatar avatar-sm" style="background-image: url({{ asset('public/tabler.admin/demo/faces/female/21.jpg')}})"></span>
                                <span class="avatar avatar-sm" style="background-image: url({{ asset('public/tabler.admin/demo/faces/female/29.jpg')}})"></span>
                                <span class="avatar avatar-sm" style="background-image: url({{ asset('public/tabler.admin/demo/faces/female/2.jpg')}})"></span>
                                <span class="avatar avatar-sm" style="background-image: url({{ asset('public/tabler.admin/demo/faces/male/34.jpg')}})"></span>
                                <span class="avatar avatar-sm">+8</span>
                        </div>
                    </div>
                </div>
                          
            </div>
      </div>
      <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
            <div class="card-body">
             <!-- START TIMELINE -->
             <div class="timeline timeline-right">
              @php
                  $tl = 1 ;
                  while($tl <= 3){
              @endphp
                       
                    <div class="message-box">
                            <div class="heading">
                                <span class="date1">08:39</span>
                            </div>                                                                                           
                            <span class="more">
                              reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur
                                sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut 
                              labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                              ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
                              reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur
                                sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </span>
                    </div>                                    

            
                @php
                $tl++;
              }
              @endphp
          </div>  
            <!-- END TIMELINE -->
            </div>
      </div>
    </div>

    <div class="card">
      <div class="card-header" id="headingThree">
        <h5 class="mb-0">
            <span class="avatar avatar-md" style="background-image: url({{ asset('public/tabler.admin/demo/faces/female/5.jpg')}})"></span>
          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
            Collapsible Group Item #3
          </button>
        </h5>
        <div class="card-options" id="cardOpt">
                <div class="d-flex align-items-center px-2">
                    <div>
                        <center><div><span class="tag tag-cyan">2 Messages</span></div></center>
                        <div class="avatar-list avatar-list-stacked" id="seen">
                                <span class="avatar avatar-sm" style="background-image: url({{ asset('public/tabler.admin/demo/faces/female/12.jpg')}})"></span>
                                <span class="avatar avatar-sm" style="background-image: url({{ asset('public/tabler.admin/demo/faces/female/21.jpg')}})"></span>
                                <span class="avatar avatar-sm">+8</span>
                        </div>
                    </div>
                </div>
                          
            </div>
      </div>

      <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
            <div class="card-body">
             <!-- START TIMELINE -->
            
                @php
                    $tl = 1 ;
                    while($tl <= 2){
                @endphp
                  <div class="message-box">
                            <div class="heading">
                                <span class="date1">08:39</span>
                            </div>                                                                                           
                            <span class="more">
                              reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur
                                sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut 
                              labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                              ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
                              reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur
                                sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </span>
                    </div>   
                  @php
                  $tl++;
                }
                @endphp
            </div>  
              <!-- END TIMELINE -->
          
      </div>
    </div>

</div> --}}





<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">  
                <span id="spanTitle"> Firstname Lastname</span>
                <span class="seen pull-right">
                    <center><div><span class="tag tag-cyan">4 Messages</span></div></center>
                            <div class="avatar-list avatar-list-stacked" id="seen">
                                    <span class="avatar avatar-sm" style="background-image: url({{ asset('public/tabler.admin/demo/faces/female/12.jpg')}})"></span>
                                    <span class="avatar avatar-sm" style="background-image: url({{ asset('public/tabler.admin/demo/faces/female/21.jpg')}})"></span>
                                    <span class="avatar avatar-sm" style="background-image: url({{ asset('public/tabler.admin/demo/faces/female/29.jpg')}})"></span>
                                    <span class="avatar avatar-sm" style="background-image: url({{ asset('public/tabler.admin/demo/faces/female/2.jpg')}})"></span>
                                    <span class="avatar avatar-sm" style="background-image: url({{ asset('public/tabler.admin/demo/faces/male/34.jpg')}})"></span>
                                    <span class="avatar avatar-sm">+8</span>
                            </div>
                </span>
                </a>
            </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne"  data-parent="#accordion">
            <div class="panel-body">

              @php
                $tl = 1 ;
                while($tl <= 5){
              @endphp
                <div class="message-box">
                    <div class="heading">
                        <span class="date1">08:39</span>
                    </div>                                                                                           
                    <span class="more">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut 
                      labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                      ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
                      reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur
                        sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                      reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur
                        sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut 
                      labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                      ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
                      reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur
                        sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </span>
                </div>                   
              @php
                $tl++;
              }
              @endphp 
        <ul class="pagination pull-right">
        <li class="page-item">
          <a class="page-link" href="#" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
            <span class="sr-only">Previous</span>
          </a>
        </li>
        <li class="page-item">
          <a class="page-link" href="#" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
            <span class="sr-only">Next</span>
          </a>
        </li>
        </ul>
        <br> 
            </div>
            
   
      
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingTwo">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    <span id="spanTitle"> Firstname Lastname</span>
                    <span class="seen pull-right">
                        <center><div><span class="tag tag-cyan">4 Messages</span></div></center>
                                <div class="avatar-list avatar-list-stacked" id="seen">
                                        <span class="avatar avatar-sm" style="background-image: url({{ asset('public/tabler.admin/demo/faces/female/12.jpg')}})"></span>
                                        <span class="avatar avatar-sm" style="background-image: url({{ asset('public/tabler.admin/demo/faces/female/21.jpg')}})"></span>
                                        <span class="avatar avatar-sm" style="background-image: url({{ asset('public/tabler.admin/demo/faces/female/29.jpg')}})"></span>
                                        <span class="avatar avatar-sm" style="background-image: url({{ asset('public/tabler.admin/demo/faces/female/2.jpg')}})"></span>
                                        <span class="avatar avatar-sm" style="background-image: url({{ asset('public/tabler.admin/demo/faces/male/34.jpg')}})"></span>
                                        <span class="avatar avatar-sm">+8</span>
                                </div>
                    </span>
                </a>
            </h4>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion" >
            <div class="panel-body">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nisl lorem, dictum id pellentesque at, vestibulum ut arcu. Curabitur erat libero, egestas eu tincidunt ac, rutrum ac justo. Vivamus condimentum laoreet lectus, blandit posuere tortor aliquam vitae. Curabitur molestie eros. </p>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingThree">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    <span id="spanTitle"> Firstname Lastname</span>
                    <span class="seen pull-right">
                        <center><div><span class="tag tag-cyan">4 Messages</span></div></center>
                                <div class="avatar-list avatar-list-stacked" id="seen">
                                        <span class="avatar avatar-sm" style="background-image: url({{ asset('public/tabler.admin/demo/faces/female/12.jpg')}})"></span>
                                        <span class="avatar avatar-sm" style="background-image: url({{ asset('public/tabler.admin/demo/faces/female/21.jpg')}})"></span>
                                        <span class="avatar avatar-sm" style="background-image: url({{ asset('public/tabler.admin/demo/faces/female/29.jpg')}})"></span>
                                        <span class="avatar avatar-sm" style="background-image: url({{ asset('public/tabler.admin/demo/faces/female/2.jpg')}})"></span>
                                        <span class="avatar avatar-sm" style="background-image: url({{ asset('public/tabler.admin/demo/faces/male/34.jpg')}})"></span>
                                        <span class="avatar avatar-sm">+8</span>
                                </div>
                    </span>
                </a>
            </h4>
        </div>
        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
            <div class="panel-body">
                    <div class="message-box">
                        <div class="heading">
                            <span class="date1">08:39</span>
                        </div>                                                                                           
                        <span class="more">
                          reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur
                            sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut 
                          labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                          ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
                          reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur
                            sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </span>
                    </div>  
              </div>
        </div>
    </div>
</div>
  @endsection

