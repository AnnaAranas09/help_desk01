@php
    $v =5;

while ($v < 9){
    
@endphp
<div class="panel panel-default">
        <div class="panel-heading" role="tab" id="heading{{$v}}">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$v}}" aria-expanded="false" aria-controls="collapse{{$v}}">
                        Hashtag and @ tag system like instagram using flutter {{$v}}
                </a>
            </h4>
        </div>
        <div id="collapse{{$v}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$v}}">
            <div class="panel-body">
                <p class="col-md-12">
                    The point of this is to update existing tags on a recipe (my recipes are the equivalent of posts on this application). Storing and displaying tags works fine,
                    but the edit is where I am having trouble. Please let me know if you see something that I dont.
                </p>
                @include('pages.admin.panel-body1')
            </div>
        </div>
</div>
@php
$v++;
}
@endphp