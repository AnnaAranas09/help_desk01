@php
    $d =13;

while ($d < 18){
    
@endphp
<div class="panel panel-default">
        <div class="panel-heading" role="tab" id="heading{{$d}}">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$d}}" aria-expanded="false" aria-controls="collapse{{$d}}">
                        Is it possible to get index of the next item from iterator?{{$d}}
                </a>
            </h4>
        </div>
        <div id="collapse{{$d}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$d}}">
            <div class="panel-body">
                <p class="col-md-12">
                    Im trying to loop through an array in one of my controllers in Laravel app 
                    and then pass it through to view. It is currently throwing error 
                    Trying to get property of non-object which tells me the array is returning as null?
                    In my controller below, I am trying to set an array as empty by default and then pass in 
                    values to that array before passing it to the view, like so:
                </p>
                @include('pages.admin.panel-body3')
            </div>
        </div>
</div>
@php
$d++;
}
@endphp