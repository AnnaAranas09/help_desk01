@extends('layouts.master')

@section('js')
    @php
    echo Packer(env('APP_URL').'/public/js/global.js');
    echo Packer(env('APP_URL').'/public/js/login.js');
    @endphp
@stop

@section('content')
<div class="page">
    <div class="page-single">
    <div class="container">
        <div class="row">
        <div class="col col-login mx-auto">
            <div class="text-center mb-6">
            <img src="{{ asset('public/tabler.admin/assets/brand/tabler.svg') }}" class="h-6" alt="">
            </div>
            <form class="card" action="" method="post">
            <div class="card-body p-6">
                <div class="card-title">Login to your account</div>
                    <div class="g-signin2" data-onsuccess="onSignIn" data-theme="dark" data-prompt="select_account consent"
                    data-onload="true"></div>
                </div>
            </div>
            </form>
        </div>
        </div>
    </div>
    </div>
</div>
@stop