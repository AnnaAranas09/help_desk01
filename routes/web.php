<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('pages.admin.dashboard');
});

Route::get('/login', 'Auth\LoginController@index');
Route::post('/login', 'Auth\LoginController@login');

Route::get('/admin/messages', 'admin\MessagesController@index');
Route::get('/admin/faq', 'admin\FAQController@index');
Route::get('/admin/issues', 'admin\IssuesController@index');

Route::get('/testpage', function(){
    return view('pages.test_client.test_page');
});
